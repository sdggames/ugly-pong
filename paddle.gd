extends CharacterBody2D
class_name Paddle

@export var player := "L"
@export var speed := 300


func  _physics_process(delta):
	if player == "L":
		if Input.is_action_pressed("left_down"):
			velocity = Vector2.DOWN * speed
		elif Input.is_action_pressed("left_up"):
			velocity = Vector2.UP * speed
		else:
			velocity = Vector2.ZERO
	
	else:
		if Input.is_action_pressed("right_down"):
			velocity = Vector2.DOWN * speed
		elif Input.is_action_pressed("right_up"):
			velocity = Vector2.UP * speed
		else:
			velocity = Vector2.ZERO
	
	move_and_slide()
