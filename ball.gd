extends CharacterBody2D

@export var vel: Vector2 = Vector2.ZERO
@export var y_scale := 100
@onready var audio_stream_player_2d = $AudioStreamPlayer2D
var will_reset := false
var starting_pos: Vector2


func _ready():
	starting_pos = position
	velocity = vel


func reset():
	will_reset = true


func _physics_process(_delta):
	if will_reset:
		position = starting_pos
		will_reset = false
		vel.y = 0
		velocity = vel
		return
	
	move_and_slide()
	
	if is_on_wall():
		audio_stream_player_2d.play()
		vel.x = -vel.x
	
	if is_on_ceiling():
		audio_stream_player_2d.play()
		vel.y = (abs(vel.y))
	
	if is_on_floor():
		audio_stream_player_2d.play()
		vel.y = -(abs(vel.y))
		
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		var paddle = collision.get_collider()
		if paddle is Paddle:
			vel.y = ((global_position.y - paddle.global_position.y) * y_scale) + paddle.velocity.y
			print("Collided with: ", paddle.name)
	
	velocity = vel
