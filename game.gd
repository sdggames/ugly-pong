extends Node
@onready var ball = $Pong/Ball
@onready var left_score = $Pong/Left_Score
@onready var right_score = $Pong/Right_Score

var left := 0
var right := 0


func _ready():
	left_score.text = str(left)
	right_score.text = str(right)


func _on_detect_left_body_entered(body):
	ball.reset()
	right += 1
	right_score.text = str(right)


func _on_detect_right_body_entered(body):
	ball.reset()
	left += 1
	left_score.text = str(left)
